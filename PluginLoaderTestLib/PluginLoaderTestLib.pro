#-------------------------------------------------
#
# Project created by QtCreator 2016-03-23T12:08:52
#
#-------------------------------------------------

QT       -= gui

TARGET = $$qtLibraryTarget(PluginLoaderTestLib)
TEMPLATE = lib
#CONFIG += staticlib

DEFINES += PLUGINLOADERTESTLIB_LIBRARY

SOURCES += pluginloadertestlib.cpp

HEADERS += pluginloadertestlib.h\
        pluginloadertestlib_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
