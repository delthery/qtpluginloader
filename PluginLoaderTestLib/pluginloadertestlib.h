#ifndef PLUGINLOADERTESTLIB_H
#define PLUGINLOADERTESTLIB_H

#include "pluginloadertestlib_global.h"

class PLUGINLOADERTESTLIBSHARED_EXPORT SolidObject : public QObject
{
    Q_OBJECT
public:
    SolidObject();

signals:
    void Signal1();

public slots:
    virtual void Slot1();
};



class Interface
{
public:
    virtual ~Interface() {}
    virtual QString Method() = 0;// {return "Interface";}
    virtual SolidObject* Instance() = 0;
};

Q_DECLARE_INTERFACE(Interface, "PLANAR.PluginLoaderTest.Interface")

#endif // PLUGINLOADERTESTLIB_H
