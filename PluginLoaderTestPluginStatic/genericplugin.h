#ifndef GENERICPLUGIN_H
#define GENERICPLUGIN_H

#include "../PluginLoaderTest/interface.h"
#include "../PluginLoaderTestLib/pluginloadertestlib.h"

class Implementation :
        public QObject,
        public Interface
        //public SolidObject
{
    Q_OBJECT
    Q_INTERFACES( Interface )

#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QGenericPluginFactoryInterface" FILE "PluginLoaderTestPlugin.json")
#endif // QT_VERSION >= 0x050000

public:
    Q_INVOKABLE Implementation(QObject *parent = 0);
    QString Method();
    SolidObject *Instance();
};
//Q_DECLARE_METATYPE(Implementation)

class SolidChild : public SolidObject
{
    Q_OBJECT
public:
    SolidChild();


    // SolidObject interface
public slots:
    void Slot1();
};

#endif // GENERICPLUGIN_H
