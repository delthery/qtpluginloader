#-------------------------------------------------
#
# Project created by QtCreator 2016-03-11T14:46:50
#
#-------------------------------------------------

QT       += core gui

TARGET = $$qtLibraryTarget(PluginLoaderTestPluginStatic)
TEMPLATE = lib

CONFIG += plugin static

#DESTDIR = $$[QT_INSTALL_PLUGINS]/generic
DESTDIR = ../plugins

SOURCES += genericplugin.cpp

HEADERS += genericplugin.h

DISTFILES += PluginLoaderTestPlugin.json

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../PluginLoaderTestLib/release/ -lPluginLoaderTestLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../PluginLoaderTestLib/debug/ -lPluginLoaderTestLibd
else:unix: LIBS += -L$$OUT_PWD/../PluginLoaderTestLib/ -lPluginLoaderTestLib

INCLUDEPATH += $$OUT_PWD/../PluginLoaderTestLib
DEPENDPATH += $$OUT_PWD/../PluginLoaderTestLib

#DLLDESTDIR = $$PWD/../PluginLoaderTest/plugins
#for(file, HEADERS) {
#    QMAKE_POST_LINK += $$QMAKE_COPY $$quote($$PWD$${file}) $$quote($$DLLDESTDIR) $$escape_expand(\\n\\t)
#}
