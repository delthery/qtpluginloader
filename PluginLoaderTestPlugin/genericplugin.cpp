#include "genericplugin.h"


Implementation::Implementation(QObject *parent) :
    QObject(parent),
    Interface()
    //SolidObject()
{
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(PluginLoaderTestPlugin, GenericPlugin)
#endif // QT_VERSION < 0x050000


QString Implementation::Method(){return "StaticImplementation";}


SolidObject *Implementation::Instance()
{
    return new SolidObject();
    //return new SolidChild();
}


SolidChild::SolidChild() : SolidObject()
{
}

void SolidChild::Slot1()
{
    qDebug() << "\033[31mSolidChild::Slot1()";
}
