TEMPLATE = subdirs


SUBDIRS +=  PluginLoaderTestLib     \
            PluginLoaderTestPlugin  \
            PluginLoaderTestPluginStatic  \
            PluginLoaderTest

PluginLoaderTestPlugin.depends = PluginLoaderTestLib
PluginLoaderTest.depends = PluginLoaderTestLib PluginLoaderTestPlugin PluginLoaderTestPluginStatic
