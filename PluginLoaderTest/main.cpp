#include <QCoreApplication>
#include <QPluginLoader>
#include <QDebug>
#include <QDir>
#include <QJsonArray>

#include "interface.h"
#include "../PluginLoaderTestLib/pluginloadertestlib.h"

Q_IMPORT_PLUGIN(Implementation)

class PluginProperty
{
public:
    class DeviceProperty
    {
        class USBProperty
        {
        public:
            USBProperty() {}

            void Read(const QJsonObject &json) {
                vid0 = json["VID0"].toInt();
                pid0 = json["PID0"].toInt();
                did0 = json["DID0"].toInt();

                vid1 = json["VID1"].toInt();
                pid1 = json["PID1"].toInt();
                did1 = json["DID1"].toInt();
            }
            quint16 vid0;
            quint16 pid0;
            quint16 did0;
            quint16 vid1;
            quint16 pid1;
            quint16 did1;
        };

        class VNAProperty
        {
        public:
            class SynthProperty
            {
            public:

                class FreqProperty
                {
                public:
                    void Read(const QJsonObject &json)
                    {
                        Min = json["Min"].toDouble();
                        Max = json["Max"].toDouble();
                        Default = json["Default"].toDouble();
                        BoundsResolution = json["BoundResolution"].toDouble();
                        GridResolution = json["GridResolution"].toDouble();
                        ReverseEnabled = json["ReverseEnabled"].toBool();
                    }
                    double Min;
                    double Max;
                    double Default;
                    double BoundsResolution;
                    double GridResolution;
                    bool ReverseEnabled;
                };
                class PowerProperty
                {
                public:
                    void Read(const QJsonObject &json)
                    {
                        Min = json["Min"].toDouble();
                        Max = json["Max"].toDouble();
                        Default = json["Default"].toDouble();
                        GridResolution = json["GridResolution"].toDouble();
                        SweepEnabled = json["ReverseEnabled"].toBool();
                    }
                    double Min;
                    double Max;
                    double Default;
                    double GridResolution;
                    bool SweepEnabled;
                };
                class BandwidthProperty
                {
                public:
                    void Read(const QJsonObject &json)
                    {
                        Min = json["Min"].toDouble();
                        Max = json["Max"].toDouble();
                        Default = json["Default"].toDouble();
                    }
                    double Min;
                    double Max;
                    double Default;
                };
                class PointsProperty
                {
                public:
                    void Read(const QJsonObject &json)
                    {
                        Max = json["Max"].toInt();
                    }
                    int Max;
                };

                SynthProperty() {}
                void Read(const QJsonObject &json)
                {
                    AverageDelay = json["AverageDelay"].toDouble();
                    FirstIf = json["FirstIF"].toDouble();
                    SecondIf = json["SecondIF"].toDouble();
                    Freq.Read(json["Frequency"].toObject());
                    Power.Read(json["Power"].toObject());
                    Bandwidth.Read(json["Bandwidth"].toObject());
                    Points.Read(json["Points"].toObject());
                }
                double AverageDelay;
                double FirstIf;
                double SecondIf;
                FreqProperty Freq;
                PowerProperty Power;
                BandwidthProperty Bandwidth;
                PointsProperty Points;
            };

            VNAProperty() {}
            void Read(const QJsonObject &json)
            {
                Firmware = json["Firmware"].toString();
                Model = json["Model"].toString();
                ROMSize = json["ROMSize"].toInt();
                Synth.Read(json["Synthesizer"].toObject());
            }

            QString Firmware;
            QString Model;
            int ROMSize;
            SynthProperty Synth;
        };

    public:
        void Read(const QJsonObject &json) {
            usb.Read(json["USB"].toObject());
            vna.Read(json["VNA"].toObject());
        }
        USBProperty usb;
        VNAProperty vna;
    };

    void Read(const QJsonObject &json)
    {
        Devices.clear();
        QJsonArray devArray = json["Devices"].toArray();
        for(int i = 0; i < devArray.size();++i) {
            QJsonObject devObject = devArray[i].toObject();
            DeviceProperty devProp;
            devProp.Read(devObject);
            Devices.append(devProp);
        }
    }
    QList<DeviceProperty> Devices;
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QVector<QStaticPlugin> plugins = QPluginLoader::staticPlugins();
    qDebug() << "Static plugins" << plugins.length();
    QObjectList objects = QPluginLoader::staticInstances();
    qDebug() << "Static instannces" << objects.length();

//    foreach(QObject* plugin, QPluginLoader::staticInstances()) {
    foreach(QStaticPlugin staticPlugin, QPluginLoader::staticPlugins()) {
        QJsonObject md = staticPlugin.metaData().value("MetaData").toObject();
        PluginProperty props;
        props.Read(md);
        qDebug() << staticPlugin.rawMetaData();
        QObject* plugin = staticPlugin.instance();

        if(plugin)
            qDebug() << plugin->metaObject()->className();

        auto interface = dynamic_cast<Interface*> (plugin);
        if(interface)
            qDebug() << "Static Plugin " << interface->Method();
    }

    qDebug() << "Lib path" << qApp->libraryPaths();
    auto pluginsDir = QDir(qApp->applicationDirPath());

//    #if defined(Q_OS_WIN)
//        if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
//            pluginsDir.cdUp();
//    #elif defined(Q_OS_MAC)
//        if (pluginsDir.dirName() == "MacOS") {
//            pluginsDir.cdUp();
//            pluginsDir.cdUp();
//            pluginsDir.cdUp();
//        }
//    #endif
    pluginsDir.cd("plugins");

    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QJsonObject md = loader.metaData().value("MetaData").toObject();
        PluginProperty props;
        props.Read(md);
        QObject *plugin = loader.instance();
        if(plugin)
            qDebug() << "File: " << fileName << " Plugin: " << plugin->metaObject()->className();

        //auto interface = dynamic_cast<Interface*> (plugin->metaObject()->newInstance());
        auto interface = dynamic_cast<Interface*> (plugin);
        if(interface) {
            qDebug() << "Interface::" << interface->Method();
            interface->Instance()->Slot1();
        }        
    }

    return 0;

    //return a.exec();
}
