#-------------------------------------------------
#
# Project created by QtCreator 2016-03-11T14:44:03
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = PluginLoaderTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    interface.h

DEFINES += LOADER_LIB

#QTPLUGIN += PluginLoaderTestPlugin
#LIBS += -L../PluginLoaderTestPlugin/
LIBS += -L$$OUT_PWD/../plugins -lPluginLoaderTestPluginStaticd

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-PluginLoaderTestPlugin-Desktop_Qt_5_5_0_MSVC2013_64bit-Debug/release/ -lPluginLoaderTestPlugin
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-PluginLoaderTestPlugin-Desktop_Qt_5_5_0_MSVC2013_64bit-Debug/debug/ -lPluginLoaderTestPlugin
#else:unix: LIBS += -L$$PWD/../build-PluginLoaderTestPlugin-Desktop_Qt_5_5_0_MSVC2013_64bit-Debug/ -lPluginLoaderTestPlugin

#INCLUDEPATH += $$PWD/../build-PluginLoaderTestPlugin-Desktop_Qt_5_5_0_MSVC2013_64bit-Debug/debug
#DEPENDPATH += $$PWD/../build-PluginLoaderTestPlugin-Desktop_Qt_5_5_0_MSVC2013_64bit-Debug/debug

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-PluginLoaderTestPlugin-Desktop_Qt_5_5_0_MSVC2013_64bit-Debug/release/libPluginLoaderTestPlugin.a
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-PluginLoaderTestPlugin-Desktop_Qt_5_5_0_MSVC2013_64bit-Debug/debug/libPluginLoaderTestPlugin.a
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-PluginLoaderTestPlugin-Desktop_Qt_5_5_0_MSVC2013_64bit-Debug/release/PluginLoaderTestPlugin.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-PluginLoaderTestPlugin-Desktop_Qt_5_5_0_MSVC2013_64bit-Debug/debug/PluginLoaderTestPlugin.lib
#else:unix: PRE_TARGETDEPS += $$PWD/../build-PluginLoaderTestPlugin-Desktop_Qt_5_5_0_MSVC2013_64bit-Debug/libPluginLoaderTestPlugin.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../PluginLoaderTestLib/release/ -lPluginLoaderTestLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../PluginLoaderTestLib/debug/ -lPluginLoaderTestLibd
else:unix: LIBS += -L$$OUT_PWD/../PluginLoaderTestLib/ -lPluginLoaderTestLib

#win32:CONFIG(release, debug|release):  LIBS += -L$$OUT_PWD/../PluginLoaderTestPlugin/release/ -lPluginLoaderTestPlugin
#else:win32:CONFIG(debug, debug|release):  LIBS += -L$$OUT_PWD/../PluginLoaderTestPlugin/debug/ -lPluginLoaderTestPlugin
#else:unix: LIBS += -L$$OUT_PWD/../PluginLoaderTestPlugin/ -lPluginLoaderTestPlugin


INCLUDEPATH += $$PWD/../PluginLoaderTestLib #$$PWD/../PluginLoaderTestPlugin
DEPENDPATH += $$PWD/../PluginLoaderTestLib #$$PWD/../PluginLoaderTestPlugin
